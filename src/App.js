import { Collections } from "./components/Collections/Collections";
import { Container } from "./components/Container/Container";
import { HalfSection } from "./components/HalfSection/HalfSection";
import { Services } from "./components/Services/Services";
import { FooterWrapper } from "./sections/FooterWrapper/FooterWrapper";
import { Gallery } from "./sections/Gallery/Gallery";
import { Header } from "./sections/Header/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Container>
        <Services />
        <HalfSection />
        <Gallery />
        <Collections />
      </Container>
      <FooterWrapper />
    </div>
  );
}

export default App;
