import React from "react";
import Logo from "../../assets/images/HOUSESALE.svg";
import PlayStore from "../../assets/images/google-play.png";
import AppStore from "../../assets/images/app-store.png";

import "./NavigationList.css";
export const NavigationList = () => {
  return (
    <div className="NavigationList">
      <div className="serving-list">
        <h4 className="serving-list__heading">Australia</h4>
        <ul className="serving-list__items">
          <li>
            {" "}
            <a href="about.html">Sydney </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Albury </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Queensland </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Adelaide </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Hobart </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Darwin </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Canberra </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Perth </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Melbourne </a>{" "}
          </li>{" "}
        </ul>
      </div>
      <div className="serving-list">
        <h4 className="serving-list__heading">UAE</h4>
        <ul className="serving-list__items">
          <li>
            {" "}
            <a href="about.html">Dubai </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Abu Dhabi </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Ajman </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html"> Al Ain </a>{" "}
          </li>{" "}
        </ul>
      </div>
      <div className="serving-list">
        <h4 className="serving-list__heading">USA</h4>
        <ul className="serving-list__items">
          <li>
            {" "}
            <a href="about.html">New York </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html"> Los Angeles</a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Chicago </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Houston </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">Philadelphia </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html">San Diego </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Washington </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Boston </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Oklahoma City </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html"> Las Vegas </a>{" "}
          </li>
        </ul>
      </div>
      <div class="serving-list last-serving">
        <h4 className="serving-list__heading">Maxico</h4>
        <ul class="serving-list__items">
          <li>
            {" "}
            <a href="about.html">Mexico City </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html">Ecatepec </a>{" "}
          </li>
          <li>
            {" "}
            <a href="about.html"> Guadalajara </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html"> Puebla City </a>{" "}
          </li>{" "}
          <li>
            {" "}
            <a href="about.html"> Ciudad Juárez </a>{" "}
          </li>{" "}
        </ul>
      </div>
      <div class="footer-contact__list">
        <div class="footer-contact__description">
          <a href="about.html">
            {" "}
            <img src={Logo} alt="logo" />
          </a>
          <p>© 2020 Housesale Technologies Ltd.</p>
          <p>1760 Dancing Dove Lane, New York, USA</p>
          <p> sales@housesales.com</p>
          <a href="about.html">
            {" "}
            <img src={PlayStore} alt="playstore" />
          </a>

          <a href="about.html">
            {" "}
            <img src={AppStore} alt="appstore" />
          </a>
        </div>
      </div>
    </div>
  );
};
