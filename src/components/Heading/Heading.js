import React from "react";

import cs from "classnames";
import "./Heading.css";
export const Heading = (props) => {
  const clasess = cs("Heading");
  return (
    <div className={clasess}>
      <h4 className="heading-title">{props.title}</h4>
    </div>
  );
};
