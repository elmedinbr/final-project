import React from "react";
import Service1 from "../../assets/images/services1.jpg";
import Service2 from "../../assets/images/services2.jpg";
import Service3 from "../../assets/images/services3.jpg";

import "./Services.css";
export const Services = () => {
  const Service = [
    {
      desc: "Same great quality. New lower prices. ",
      image: Service1,
    },
    {
      desc: "Everyday Essentials, High quality affordable  ",
      image: Service2,
    },
    {
      desc: "Join the makeover journey ",
      image: Service3,
    },
  ];
  return (
    <div className="services-section">
      {Service.map((item) => {
        return (
          <div className="services-section__item">
            <a href="about.html">
              <div className="services-section__image">
                <img src={item.image} alt="Services1" />
              </div>
              <div className="services-section__description">
                <h4>{item.desc}</h4>
              </div>
            </a>
          </div>
        );
      })}
    </div>
  );
};
