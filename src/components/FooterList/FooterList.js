import React from "react";

import "./FooterList.css";
export const FooterList = () => {
  return (
    <div className="footer-wrapper__navigation">
      <ul className="navigation-list">
        <li>
          {" "}
          <a href="about.html">Delivery Service </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Assembly Service </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Customer Service </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Contact Us </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Near Me </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Careers </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Gift Cards </a>
        </li>
        <li>
          {" "}
          <a href="about.html">Contact Us </a>
        </li>
      </ul>
    </div>
  );
};
