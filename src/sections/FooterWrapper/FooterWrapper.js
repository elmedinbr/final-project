import React from "react";
import { Container } from "../../components/Container/Container";

import "./FooterWrapper.css";
import { FooterList } from "../../components/FooterList/FooterList";
import { NavigationList } from "../../components/NavigationList/NavigationList";
export const FooterWrapper = () => {
  return (
    <div className="FooterWrapper">
      <Container>
        <FooterList />
        <NavigationList />
      </Container>
    </div>
  );
};
