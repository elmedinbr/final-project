import React from "react";
import { Heading } from "../../components/Heading/Heading";
import First from "../../assets/images/first.png";
import Second from "../../assets/images/second.png";
import Third from "../../assets/images/third.png";
import Fourth from "../../assets/images/fourth.png";
import Fifth from "../../assets/images/fifth.png";
import Sixth from "../../assets/images/sixth.png";

import "./Gallery.css";
export const Gallery = () => {
  const Gallery = [
    {
      img: First,
    },
    {
      img: Second,
    },
    {
      img: Third,
    },
    {
      img: Fourth,
    },
    {
      img: Second,
    },
    {
      img: Fifth,
    },
    {
      img: Sixth,
    },
    {
      img: Second,
    },
  ];
  return (
    <div className="Gallery">
      <Heading title="More ideas and inspiration" />
      <div className="gallery-items">
        {Gallery.map((item) => {
          return (
            <div className="gallery-content">
              <div className="gallery-images">
                <img src={item.img} alt="all-img" />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
